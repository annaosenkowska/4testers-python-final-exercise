class Product:
    def __init__(self, name, unit_price, quantity=1):
        self.name = name
        self.unit_price = unit_price
        self.quantity = quantity

    def get_price(self):
        price = self.unit_price * self.quantity
        return price


class Order:
    def __init__(self, customer_email):
        self.customer_email = customer_email
        self.products = []
        self.purchased = False

    def add_product(self, product: Product):
        self.products.append(product)

    def get_total_price(self):
        list = []
        for product in self.products:
            list.append(product.get_price())
        return sum(list)

    def get_total_quantity_of_products(self):
        list = []
        for product in self.products:
            list.append(product.quantity)
        return sum(list)

    def purchase(self):
        if self.get_total_quantity_of_products() != 0:
            self.purchased = True




